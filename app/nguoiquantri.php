<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nguoiquantri extends Model
{
    protected $table = 'nguoiquantri';
    protected $primaryKey = 'idqt';
    public $timestamps = false;
}
