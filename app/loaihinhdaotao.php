<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loaihinhdaotao extends Model
{
    protected $table = 'loaihinhdaotao';
    protected $primaryKey = 'idlhdt';
    public $timestamps = false;
}
