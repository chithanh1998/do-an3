<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loaidichvu extends Model
{
    protected $table = 'loaidichvu';
    protected $primaryKey = 'idloaidv';
    public $timestamps = false;
}
