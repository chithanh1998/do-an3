<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chuyennganh extends Model
{
    protected $table = 'chuyennganh';
    protected $primaryKey = 'idcn';
    public $timestamps = false;
}
