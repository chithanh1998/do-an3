<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class khoa extends Model
{
    protected $table = 'khoa';
    protected $primaryKey = 'idkhoa';
    public $timestamps = false;
}
