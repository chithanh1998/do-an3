<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loaitintuc extends Model
{
    protected $table = 'loaitintuc';
    protected $primaryKey = 'idloaitt';
    public $timestamps = false;
}
