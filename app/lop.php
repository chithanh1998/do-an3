<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lop extends Model
{
    protected $table = 'lop';
    protected $primaryKey = 'idlop';
    public $timestamps = false;
}
