<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dichvu extends Model
{
    protected $table = 'dichvu';
    protected $primaryKey = 'iddv';
    public $timestamps = false;
}
