<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\taikhoan;
use App\nguoiquantri;
use App\sinhvien;
use Illuminate\Support\Str;

class TaikhoanController extends Controller
{
    public function get_dangnhap()
    {
        if(session()->get('quyen') == "admin"){
            return redirect(route('dsqt'));
        } elseif(session()->get('quyen') == "sv"){
            return redirect(route('dstintuc'));
        } else {
            return view('dangnhap');
        }
    }
    public function post_dangnhap(Request $request)
    {
        $tk = taikhoan::where('tentk',$request->tentaikhoan)->where('matkhau',md5($request->matkhau))->get();
        if($tk->count() > 0){
            if($tk->first()->quyen == 1){
                session()->put('quyen','admin');
                $admin = nguoiquantri::where('idtk',$tk->first()->idtk)->first();
                session()->put('idqt',$admin->idqt);
                session()->put('avatar',$admin->anh);
                session()->put('ten',$admin->hoten);
                return 'admin';
            } else {
                session()->put('quyen','sv');
                $sv = sinhvien::where('idtk',$tk->first()->idtk)->first();
                session()->put('idsv',$sv->idsv);
                session()->put('avatar',$sv->anh);
                session()->put('ten',$sv->hoten);
                return 'ok';
            }
        } else {
            return 'err';
        }
    }
    public function dangxuat(){
        session()->flush();
        return redirect('/');
    }
    public function get_thongtincanhan()
    {
        $quyen = session('quyen');
        if($quyen == "admin"){
            $tk = nguoiquantri::find(session('idqt'));
            return view('ttcn',['tk'=>$tk]);
        } else {
            $tk = sinhvien::find(session('idsv'));
            return view('ttcn',['tk'=>$tk]);
        }
    }
    public function post_thongtincanhan(Request $request)
    {
        $quyen = session('quyen');
        if($quyen == "admin"){
            $tk = nguoiquantri::find(session('idqt'));
            $tk->hoten = $request->hoten;
            $tk->ngaysinh = $request->ngaysinh;
            $tk->quequan = $request->quequan;
            $tk->diachi = $request->diachi;
            $tk->gioitinh = $request->gioitinh;
            $tk->dantoc = $request->dantoc;
            $tk->sdt = $request->sdt;
            $tk->email = $request->email;
            $tk->save();
            return back()->with('succ',"Lưu thành công");
        } else {
            $tk = sinhvien::find(session('idsv'));
            $tk->hoten = $request->hoten;
            $tk->ngaysinh = $request->ngaysinh;
            $tk->quequan = $request->quequan;
            $tk->diachi = $request->diachi;
            $tk->gioitinh = $request->gioitinh;
            $tk->dantoc = $request->dantoc;
            $tk->sdt = $request->sdt;
            $tk->email = $request->email;
            $tk->cmnd = $request->cmnd;
            $tk->save();
            return back()->with('succ',"Lưu thành công");
        }
    }
    public function doiavatar(Request $request)
    {
        if($request->hasFile('anh')){
            $file = $request->file('anh');
            $name = Str::random(5).'_'.$file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/upload/avatar';
            $file->move($destination, $name);
            $quyen = session('quyen');
            if($quyen == "admin"){
                $tk = nguoiquantri::find(session('idqt'));
                $tk->anh = $name;
                $tk->save();
            } else {
                $tk = sinhvien::find(session('idsv'));
                $tk->anh = $name;
                $tk->save();
            }
            session()->put('avatar',$name);
            return back()->with('succ','Đổi thành công');
        }
    }
}
