<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\chuyennganh;
use App\sinhvien;
use App\khoa;

class ChuyennganhController extends Controller
{
    public function ds()
    {
        $cn = chuyennganh::leftjoin('khoa','chuyennganh.idkhoa','=','khoa.idkhoa')->get();
        $khoa = khoa::all();
        return view('chuyennganh.ds',['chuyennganh'=>$cn,'khoa'=>$khoa]);
    }
    public function thongtin(Request $request)
    {
        $cn = chuyennganh::find($request->id);
        return json_encode($cn);
    }
    public function post_them(Request $request)
    {
        $check = chuyennganh::where('tenchuyennganh',$request->tenchuyennganh)->where('idkhoa',$request->khoa)->count();
        if($check > 0){
            return 'err';
        } else {
            $cn = new chuyennganh;
            $cn->tenchuyennganh = $request->tenchuyennganh;
            $cn->idkhoa = $request->khoa;
            $cn->save();
            return 'ok';
        }
    }
    public function post_sua(Request $request)
    {
        $check = chuyennganh::where('tenchuyennganh',$request->tenchuyennganh)->where('idkhoa',$request->khoa)->count();
        if($check > 0){
            return 'err';
        } else {
            $cn = chuyennganh::where('idcn',$request->idchuyennganh)->first();
            $cn->tenchuyennganh = $request->tenchuyennganh;
            $cn->idkhoa = $request->khoa;
            $cn->save();
            return 'ok';
        }
    }
    public function xoa($id)
    {
        $check = sinhvien::where('idchuyennganh',$id)->count();
        if($check > 0){
            return back()->with('err','Chuyên ngành này đang có sinh viên');
        } else {
            $cn = chuyennganh::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
