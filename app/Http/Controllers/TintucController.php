<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tintuc;;
use App\loaitintuc;

class TintucController extends Controller
{
    public function ds()
    {
        $tt = tintuc::all();
        return view('tintuc.ds',['tintuc'=>$tt]);
    }
    public function get_them()
    {
        $ltt = loaitintuc::all();
        return view('tintuc.them',['loaitt'=>$ltt]);
    }
    public function post_them(Request $request)
    {
        $tt = new tintuc;
        $tt->tieude = $request->tieude;
        $tt->noidung = $request->noidung;
        $tt->idloaitt = $request->loaitt;
        $tt->save();
        return back()->with('succ','Thêm thành công');
    }
    public function get_sua($id)
    {
        $tt = tintuc::find($id);
        $ltt = loaitintuc::all();
        return view('tintuc.sua',['tintuc'=>$tt,'loaitt'=>$ltt]);
    }
    public function post_sua($id, Request $request)
    {
        $tt = tintuc::find($id);
        $tt->tieude = $request->tieude;
        $tt->noidung = $request->noidung;
        $tt->idloaitt = $request->loaitt;
        $tt->save();
        return back()->with('succ','Sửa thành công');
    }
    public function xoa($id)
    {
        $tt = tintuc::find($id)->delete();
        return back()->with('succ','Xóa thành công');
    }
    public function dsloaitt()
    {
        $ltt = loaitintuc::all();
        return view('tintuc.loaitt',['loaitt'=>$ltt]);
    }
    public function thongtinloaitt(Request $request)
    {
        $ltt = loaitintuc::find($request->id);
        return json_encode($ltt);
    }
    public function post_themloaitt(Request $request)
    {
        $check = loaitintuc::where('tenloaitt',$request->loaitt)->count();
        if($check > 0){
            return 'err';
        } else {
            $ltt = new loaitintuc;
            $ltt->tenloaitt = $request->loaitt;
            $ltt->save();
            return 'ok';
        }
    }
    public function post_sualoaitt(Request $request)
    {
        $check = loaitintuc::where('tenloaitt',$request->loaitt)->count();
        if($check > 0){
            return 'err';
        } else {
            $ltt = loaitintuc::find($request->idloaitt);
            $ltt->tenloaitt = $request->loaitt;
            $ltt->save();
            return 'ok';
        }
    }
    public function xoaloaitt($id)
    {
        $check = tintuc::where('idloaitt',$id)->count();
        if($check > 0){
            return back()->with('err','Loại tin tức này đang có tin tức');
        } else {
            $ltt = loaitintuc::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
