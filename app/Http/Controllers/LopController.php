<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\lop;
use App\khoa;
use App\sinhvien;

class LopController extends Controller
{
    public function ds()
    {
        $lop = lop::leftjoin('khoa','lop.idkhoa','=','khoa.idkhoa')->get();
        $khoa = khoa::all();
        return view('lop.ds',['lop'=>$lop,'khoa'=>$khoa]);
    }
    public function thongtin(Request $request)
    {
        $lop = lop::find($request->id);
        return json_encode($lop);
    }
    public function post_them(Request $request)
    {
        $check = lop::where('tenlop',$request->tenlop)->where('idkhoa',$request->khoa)->count();
        if($check > 0){
            return 'err';
        } else {
            $lop = new lop;
            $lop->tenlop = $request->tenlop;
            $lop->idkhoa = $request->khoa;
            $lop->save();
            return 'ok';
        }
    }
    public function post_sua(Request $request)
    {
        $check = lop::where('tenlop',$request->tenlop)->where('idkhoa',$request->khoa)->count();
        if($check > 0){
            return 'err';
        } else {
            $lop = lop::find($request->idlop);
            $lop->tenlop = $request->tenlop;
            $lop->idkhoa = $request->khoa;
            $lop->save();
            return 'ok';
        }
    }
    public function xoa($id)
    {
        $check = sinhvien::where('idlop',$id)->count();
        if($check > 0){
            return back()->with('err','Lớp này đang có sinh viên');
        } else {
            $lop = lop::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
