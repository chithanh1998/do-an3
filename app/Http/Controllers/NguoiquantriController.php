<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\nguoiquantri;
use App\taikhoan;

class NguoiquantriController extends Controller
{
    public function ds()
    {
        $qt = nguoiquantri::all();
        return view('quantri.ds',['qt'=>$qt]);
    }
    public function post_them(Request $request)
    {
        $check = taikhoan::where('tentk',$request->tentk)->count();
        if($check > 0){
            return 'err';
        } else {
            $tk = new taikhoan;
            $tk->tentk = $request->tentk;
            $tk->matkhau = md5($request->matkhau);
            $tk->quyen = 1;
            $tk->save();
            $qt = new nguoiquantri;
            $qt->hoten = $request->hoten;
            $qt->ngaysinh = $request->ngaysinh;
            $qt->quequan = $request->quequan;
            $qt->diachi = $request->diachi;
            $qt->gioitinh = $request->gioitinh;
            $qt->dantoc = $request->dantoc;
            $qt->sdt = $request->sdt;
            $qt->email = $request->email;
            $qt->idtk = $tk->idtk;
            $qt->save();
            return 'ok';
        }
    }
    public function thongtin(Request $request)
    {
        $qt = nguoiquantri::find($request->id);
        return json_encode($qt);
    }
    public function post_sua(Request $request)
    {
        $qt = nguoiquantri::find($request->idqt);
        $qt->hoten = $request->hoten;
        $qt->ngaysinh = $request->ngaysinh;
        $qt->quequan = $request->quequan;
        $qt->diachi = $request->diachi;
        $qt->gioitinh = $request->gioitinh;
        $qt->dantoc = $request->dantoc;
        $qt->sdt = $request->sdt;
        $qt->email = $request->email;
        $qt->save();
        return 'ok';
    }
    public function xoa($id)
    {
        $qt = nguoiquantri::find($id);
        $tk = taikhoan::find($qt->idtk)->delete();
        $qt->delete();
        return back()->with('succ','Xóa thành công');
    }
}
