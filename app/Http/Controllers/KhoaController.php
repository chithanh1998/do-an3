<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\khoa;
use App\sinhvien;

class KhoaController extends Controller
{
    public function ds()
    {
        $khoa = khoa::all();
        return view('khoa.ds',['khoa'=>$khoa]);
    }
    public function thongtin(Request $request)
    {
        $khoa = khoa::find($request->id);
        return json_encode($khoa);
    }
    public function post_them(Request $request)
    {
        $check = khoa::where('tenkhoa',$request->tenkhoa)->count();
        if($check > 0){
            return 'err';
        } else {
            $khoa = new khoa;
            $khoa->tenkhoa = $request->tenkhoa;
            $khoa->save();
            return 'ok';
        }
    }
    public function post_sua(Request $request)
    {
        $check = khoa::where('tenkhoa',$request->tenkhoa)->count();
        if($check > 0){
            return 'err';
        } else {
            $khoa = khoa::find($request->idkhoa);
            $khoa->tenkhoa = $request->tenkhoa;
            $khoa->save();
            return 'ok';
        }
    }
    public function xoa($id)
    {
        $check = sinhvien::where('idkhoa',$id)->count();
        if($check > 0){
            return back()->with('err','Khoa này đang có sinh viên');
        } else {
            $khoa = khoa::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
