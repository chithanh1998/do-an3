<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sinhvien;
use App\khoa;
use App\lop;
use App\chuyennganh;
use App\khoahoc;
use App\loaihinhdaotao;

class TimkiemController extends Controller
{
    public function get_tk()
    {
        return view('timkiem');
    }
    public function post_tk(Request $request)
    {
        $sv = sinhvien::leftjoin('khoa','sinhvien.idkhoa','=','khoa.idkhoa')->leftjoin('lop','sinhvien.idlop','=','lop.idlop')->leftjoin('chuyennganh','sinhvien.idchuyennganh','=','chuyennganh.idcn')->where('sinhvien.hoten','like','%'.$request->hoten.'%')->where('lop.tenlop','like','%'.$request->lop.'%')->where('chuyennganh.tenchuyennganh','like','%'.$request->chuyennganh.'%')->where('khoa.tenkhoa','like','%'.$request->khoa.'%')->get();
        $lop = lop::all();
        $khoa = khoa::all();
        $chuyennganh = chuyennganh::all();
        $khoahoc = khoahoc::all();
        $loaihinhdaotao = loaihinhdaotao::all();
        return view('timkiem',['sv'=>$sv,'lop'=>$lop,'khoa'=>$khoa,'chuyennganh'=>$chuyennganh,'loaihinhdaotao'=>$loaihinhdaotao,'khoahoc'=>$khoahoc]);

    }
}
