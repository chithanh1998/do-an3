<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dichvu;
use App\loaidichvu;
use App\dondangky;

class DichvuController extends Controller
{
    public function ds()
    {
        $dv = dichvu::leftjoin('loaidichvu','dichvu.idloaidv','=','loaidichvu.idloaidv')->get();
        $ldv = loaidichvu::all();;
        return view('dichvu.dv',['dv'=>$dv,'loaidv'=>$ldv]);
    }
    public function thongtin(Request $request)
    {
        $dv = dichvu::find($request->id);
        return json_encode($dv);
    }
    public function post_them(Request $request)
    {
        $check = dichvu::where('tendv',$request->tendv)->where('idloaidv',$request->loaidv)->count();
        if($check > 0){
            return 'err';
        } else {
            $dv = new dichvu;
            $dv->tendv = $request->tendv;
            $dv->idloaidv = $request->loaidv;
            $dv->save();
            return 'ok';
        }
    }
    public function post_sua(Request $request)
    {
        $check = dichvu::where('tendv',$request->tendv)->where('idloaidv',$request->loaidv)->count();
        if($check > 0){
            return 'err';
        } else {
            $dv = dichvu::find($request->iddv);
            $dv->tendv = $request->tendv;
            $dv->idloaidv = $request->loaidv;
            $dv->save();
            return 'ok';
        }
    }
    public function xoa($id)
    {
        $check = dondangky::where('iddv',$id)->count();
        if($check > 0){
            return back()->with('err','Dịch vụ này đang có đơn đăng ký');
        } else {
            $dv = dichvu::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
    public function dsloaidv()
    {
        $ldv = loaidichvu::all();
        return view('dichvu.loaidv',['loaidv'=>$ldv]);
    }
    public function thongtinloaidv(Request $request)
    {
        $ldv = loaidichvu::find($request->id);
        return json_encode($ldv);
    }
    public function post_themloaidv(Request $request)
    {
        $check = loaidichvu::where('tenloaidv',$request->loaidv)->count();
        if($check > 0){
            return 'err';
        } else {
            $ldv = new loaidichvu;
            $ldv->tenloaidv = $request->loaidv;
            $ldv->save();
            return 'ok';
        }
    }
    public function post_sualoaidv(Request $request)
    {
        $check = loaidichvu::where('tenloaidv',$request->loaidv)->count();
        if($check > 0){
            return 'err';
        } else {
            $ldv = loaidichvu::find($request->idloaidv);
            $ldv->tenloaidv = $request->loaidv;
            $ldv->save();
            return 'ok';
        }
    }
    public function xoaloaidv($id)
    {
        $check = dichvu::where('idloaidv',$id)->count();
        if($check > 0){
            return back()->with('err','Loại dịch vụ này đang có dịch vụ');
        } else {
            $ldv = loaidichvu::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
