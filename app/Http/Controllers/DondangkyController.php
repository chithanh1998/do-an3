<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sinhvien;
use App\khoa;
use App\lop;
use App\chuyennganh;
use App\khoahoc;
use App\loaihinhdaotao;
use App\dondangky;
use App\dichvu;

class DondangkyController extends Controller
{
    public function ds()
    {
        $lop = lop::all();
        $khoa = khoa::all();
        $chuyennganh = chuyennganh::all();
        $khoahoc = khoahoc::all();
        $loaihinhdaotao = loaihinhdaotao::all();
        $dv = dichvu::all();
        $ddk = dondangky::leftjoin('sinhvien','dondangky.idsv','=','sinhvien.idsv')->leftjoin('dichvu','dondangky.iddv','=','dichvu.iddv')->latest()->get();
        return view('dondangky.ds',['ddk'=>$ddk,'lop'=>$lop,'khoa'=>$khoa,'chuyennganh'=>$chuyennganh,'loaihinhdaotao'=>$loaihinhdaotao,'khoahoc'=>$khoahoc,'dv'=>$dv]);
    }
    public function dangxuly($id)
    {
        $ddk = dondangky::find($id);
        $ddk->trangthai = 1;
        $ddk->save();
        return back()->with('succ','Thành công');
    }
    public function hoanthanh($id)
    {
        $ddk = dondangky::find($id);
        $ddk->trangthai = 2;
        $ddk->save();
        return back()->with('succ','Thành công');
    }
    public function thongtin(Request $request)
    {
        $ddk = dondangky::where('iddondangky',$request->id)->leftjoin('sinhvien','dondangky.idsv','=','sinhvien.idsv')->first();
        return json_encode($ddk);
    }
}
