<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sinhvien;
use App\taikhoan;
use App\khoa;
use App\lop;
use App\chuyennganh;
use App\khoahoc;
use App\loaihinhdaotao;
use App\loaidichvu;
use App\dichvu;
use App\tintuc;
use App\dondangky;

class SinhvienController extends Controller
{
    public function ds()
    {
        $sv = sinhvien::leftjoin('khoa','sinhvien.idkhoa','=','khoa.idkhoa')->leftjoin('lop','sinhvien.idlop','=','lop.idlop')->leftjoin('loaihinhdaotao','sinhvien.idloaihinhdaotao','=','loaihinhdaotao.idlhdt')->leftjoin('khoahoc','sinhvien.idkhoahoc','=','khoahoc.idkh')->get();
        $lop = lop::all();
        $khoa = khoa::all();
        $chuyennganh = chuyennganh::all();
        $khoahoc = khoahoc::all();
        $loaihinhdaotao = loaihinhdaotao::all();
        return view('sinhvien.ds',['sv'=>$sv,'lop'=>$lop,'khoa'=>$khoa,'chuyennganh'=>$chuyennganh,'loaihinhdaotao'=>$loaihinhdaotao,'khoahoc'=>$khoahoc]);
    }
    public function post_them(Request $request)
    {
        $check = taikhoan::where('tentk',$request->tentk)->count();
        if($check > 0){
            return 'err';
        } else {
            $tk = new taikhoan;
            $tk->tentk = $request->tentk;
            $tk->matkhau = md5($request->matkhau);
            $tk->quyen = 2;
            $tk->save();
            $sv = new sinhvien;
            $sv->hoten = $request->hoten;
            $sv->ngaysinh = $request->ngaysinh;
            $sv->quequan = $request->quequan;
            $sv->diachi = $request->diachi;
            $sv->gioitinh = $request->gioitinh;
            $sv->dantoc = $request->dantoc;
            $sv->sdt = $request->sdt;
            $sv->email = $request->email;
            $sv->cmnd = $request->cmnd;
            $sv->idkhoa = $request->khoa;
            $sv->idlop = $request->lop;
            $sv->idchuyennganh = $request->chuyennganh;
            $sv->idkhoahoc = $request->khoahoc;
            $sv->idloaihinhdaotao = $request->lhdt;
            $sv->idtk = $tk->idtk;
            $sv->save();
            return 'ok';
        }
    }
    public function thongtin(Request $request)
    {
        $sv = sinhvien::find($request->id);
        return json_encode($sv);
    }
    public function post_sua(Request $request)
    {
        $sv = sinhvien::find($request->idsv);
        $sv->hoten = $request->hoten;
        $sv->ngaysinh = $request->ngaysinh;
        $sv->quequan = $request->quequan;
        $sv->diachi = $request->diachi;
        $sv->gioitinh = $request->gioitinh;
        $sv->dantoc = $request->dantoc;
        $sv->sdt = $request->sdt;
        $sv->email = $request->email;
        $sv->cmnd = $request->cmnd;
        $sv->idkhoa = $request->khoa;
        $sv->idlop = $request->lop;
        $sv->idchuyennganh = $request->chuyennganh;
        $sv->idkhoahoc = $request->khoahoc;
        $sv->idloaihinhdaotao = $request->lhdt;
        $sv->save();
        return 'ok';
    }
    public function xoa($id)
    {
        $sv = sinhvien::find($id);
        $tk = taikhoan::find($sv->idtk)->delete();
        $sv->delete();
        return back()->with('succ','Xóa thành công');
    }
    public function dsloaidv()
    {
        $ldv = loaidichvu::all();
        return view('sinhvien.xemloaidv',['loaidv'=>$ldv]);;
    }
    public function dsdv($id_ldv)
    {
        $dv = dichvu::where('idloaidv',$id_ldv)->get();
        $lop = lop::all();
        $khoa = khoa::all();
        $chuyennganh = chuyennganh::all();
        $khoahoc = khoahoc::all();
        $loaihinhdaotao = loaihinhdaotao::all();
        $sv = sinhvien::find(session('idsv'));
        return view('sinhvien.xemdv',['dv'=>$dv,'lop'=>$lop,'khoa'=>$khoa,'chuyennganh'=>$chuyennganh,'loaihinhdaotao'=>$loaihinhdaotao,'khoahoc'=>$khoahoc,'sv'=>$sv]);
    }
    public function dstintuc()
    {
        $tt = tintuc::latest()->get();
        return view('sinhvien.xemdstintuc',['tintuc'=>$tt]);
    }
    public function xemtintuc($id)
    {
        $tt = tintuc::find($id);
        return view('sinhvien.xemtintuc',['tintuc'=>$tt]);
    }
    public function dkdv(Request $request)
    {
        $ddk = new dondangky;
        $ddk->iddv = $request->iddv;
        $ddk->idsv = session()->get('idsv');
        $ddk->lydolamdon = $request->lydo;
        $ddk->save();
        return 'ok';
    }
    public function dsddk()
    {
        $ddk = dondangky::where('idsv',session()->get('idsv'))->leftjoin('dichvu','dondangky.iddv','=','dichvu.iddv')->latest()->get();
        return view('sinhvien.dsdondangky',['ddk'=>$ddk]);
    }
    public function xoaddk($id)
    {
        $ddk = dondangky::find($id)->delete();
        return back()->with('succ','Xóa thành công');
    }
}
