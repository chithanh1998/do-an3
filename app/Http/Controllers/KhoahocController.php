<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\khoahoc;
use App\sinhvien;

class KhoahocController extends Controller
{
    public function ds()
    {
        $khoahoc = khoahoc::all();
        return view('khoahoc.ds',['khoahoc'=>$khoahoc]);
    }
    public function thongtin(Request $request)
    {
        $khoahoc = khoahoc::find($request->id);
        return json_encode($khoahoc);
    }
    public function post_them(Request $request)
    {
        $check = khoahoc::where('nambatdau',$request->nambatdau)->where('namketthuc',$request->namketthuc)->count();
        if($check > 0){
            return 'err';
        } else {
            $khoahoc = new khoahoc;
            $khoahoc->nambatdau = $request->nambatdau;
            $khoahoc->namketthuc = $request->namketthuc;
            $khoahoc->save();
            return 'ok';
        }
    }
    public function post_sua(Request $request)
    {
        $check = khoahoc::where('nambatdau',$request->nambatdau)->where('namketthuc',$request->namketthuc)->count();
        if($check > 0){
            return 'err';
        } else {
            $khoahoc = khoahoc::find($request->idkhoahoc);
            $khoahoc->nambatdau = $request->nambatdau;
            $khoahoc->namketthuc = $request->namketthuc;
            $khoahoc->save();
            return 'ok';
        }
    }
    public function xoa($id)
    {
        $check = sinhvien::where('idkhoahoc',$id)->count();
        if($check > 0){
            return back()->with('err','Khóa học này đang có sinh viên');
        } else {
            $khoahoc = khoahoc::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
