<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\loaihinhdaotao;
use App\sinhvien;

class LoaihinhdaotaoController extends Controller
{
    public function ds()
    {
        $lhdt = loaihinhdaotao::all();
        return view('loaihinhdaotao.ds',['lhdt'=>$lhdt]);
    }
    public function thongtin(Request $request)
    {
        $lhdt = loaihinhdaotao::find($request->id);
        return json_encode($lhdt);
    }
    public function post_them(Request $request)
    {
        $check = loaihinhdaotao::where('tenlhdt',$request->lhdt)->count();
        if($check > 0){
            return 'err';
        } else {
            $lhdt = new loaihinhdaotao;
            $lhdt->tenlhdt = $request->lhdt;
            $lhdt->save();
            return 'ok';
        }
    }
    public function post_sua(Request $request)
    {
        $check = loaihinhdaotao::where('tenlhdt',$request->lhdt)->count();
        if($check > 0){
            return 'err';
        } else {
            $lhdt = loaihinhdaotao::find($request->idlhdt);
            $lhdt->tenlhdt = $request->lhdt;
            $lhdt->save();
            return 'ok';
        }
    }
    public function xoa($id)
    {
        $check = sinhvien::where('idloaihinhdaotao',$id)->count();
        if($check > 0){
            return back()->with('err','Loại hình đào tạo này đang có sinh viên');
        } else {
            $lhdt = loaihinhdaotao::find($id)->delete();
            return back()->with('succ','Xóa thành công');
        }
    }
}
