<?php

namespace App\Http\Middleware;

use Closure;

class dangnhap
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty(session('quyen'))){
            return $next($request);
        } else {
        return redirect('/')->with('err','Đăng nhập để tiếp tục');
        }
    }
}
