<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class khoahoc extends Model
{
    protected $table = 'khoahoc';
    protected $primaryKey = 'idkh';
    public $timestamps = false;
}
