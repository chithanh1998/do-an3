-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2020 at 04:12 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `da3`
--

-- --------------------------------------------------------

--
-- Table structure for table `chuyennganh`
--

CREATE TABLE `chuyennganh` (
  `idcn` int(11) NOT NULL,
  `tenchuyennganh` text COLLATE utf8_unicode_ci NOT NULL,
  `idkhoa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chuyennganh`
--

INSERT INTO `chuyennganh` (`idcn`, `tenchuyennganh`, `idkhoa`) VALUES
(1, 'tesst', 1),
(2, 'tét1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dichvu`
--

CREATE TABLE `dichvu` (
  `iddv` int(11) NOT NULL,
  `tendv` text COLLATE utf8_unicode_ci NOT NULL,
  `idloaidv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dichvu`
--

INSERT INTO `dichvu` (`iddv`, `tendv`, `idloaidv`) VALUES
(1, 'test', 1),
(2, 'test2a', 2);

-- --------------------------------------------------------

--
-- Table structure for table `dondangky`
--

CREATE TABLE `dondangky` (
  `iddondangky` int(11) NOT NULL,
  `iddv` int(11) NOT NULL,
  `idsv` int(11) NOT NULL,
  `lydolamdon` text COLLATE utf8_unicode_ci NOT NULL,
  `trangthai` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:đang chờ | 1:đang xử lý | 2:hoan thành',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `dondangky`
--

INSERT INTO `dondangky` (`iddondangky`, `iddv`, `idsv`, `lydolamdon`, `trangthai`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 'a', 0, '2020-04-06 17:40:12', '2020-04-06 17:40:12'),
(4, 1, 1, 'sdfagagag', 0, '2020-04-09 17:21:29', '2020-04-09 17:21:29'),
(5, 1, 1, 'w', 0, '2020-04-10 15:00:51', '2020-04-10 15:00:51');

-- --------------------------------------------------------

--
-- Table structure for table `khoa`
--

CREATE TABLE `khoa` (
  `idkhoa` int(11) NOT NULL,
  `tenkhoa` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khoa`
--

INSERT INTO `khoa` (`idkhoa`, `tenkhoa`) VALUES
(1, 'Test1');

-- --------------------------------------------------------

--
-- Table structure for table `khoahoc`
--

CREATE TABLE `khoahoc` (
  `idkh` int(11) NOT NULL,
  `nambatdau` int(11) NOT NULL,
  `namketthuc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khoahoc`
--

INSERT INTO `khoahoc` (`idkh`, `nambatdau`, `namketthuc`) VALUES
(1, 2016, 2020);

-- --------------------------------------------------------

--
-- Table structure for table `loaidichvu`
--

CREATE TABLE `loaidichvu` (
  `idloaidv` int(11) NOT NULL,
  `tenloaidv` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loaidichvu`
--

INSERT INTO `loaidichvu` (`idloaidv`, `tenloaidv`) VALUES
(1, '1'),
(2, '2a');

-- --------------------------------------------------------

--
-- Table structure for table `loaihinhdaotao`
--

CREATE TABLE `loaihinhdaotao` (
  `idlhdt` int(11) NOT NULL,
  `tenlhdt` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loaihinhdaotao`
--

INSERT INTO `loaihinhdaotao` (`idlhdt`, `tenlhdt`) VALUES
(1, 'test'),
(2, 'a1');

-- --------------------------------------------------------

--
-- Table structure for table `loaitintuc`
--

CREATE TABLE `loaitintuc` (
  `idloaitt` int(11) NOT NULL,
  `tenloaitt` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `loaitintuc`
--

INSERT INTO `loaitintuc` (`idloaitt`, `tenloaitt`) VALUES
(1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `lop`
--

CREATE TABLE `lop` (
  `idlop` int(11) NOT NULL,
  `tenlop` text COLLATE utf8_unicode_ci NOT NULL,
  `idkhoa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lop`
--

INSERT INTO `lop` (`idlop`, `tenlop`, `idkhoa`) VALUES
(1, 'A1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nguoiquantri`
--

CREATE TABLE `nguoiquantri` (
  `idqt` int(11) NOT NULL,
  `hoten` text COLLATE utf8_unicode_ci NOT NULL,
  `ngaysinh` date NOT NULL,
  `quequan` text COLLATE utf8_unicode_ci NOT NULL,
  `diachi` text COLLATE utf8_unicode_ci NOT NULL,
  `anh` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '''default.png''',
  `gioitinh` tinyint(4) NOT NULL COMMENT '1:Nam | 2: Nu',
  `dantoc` text COLLATE utf8_unicode_ci NOT NULL,
  `sdt` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `idtk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nguoiquantri`
--

INSERT INTO `nguoiquantri` (`idqt`, `hoten`, `ngaysinh`, `quequan`, `diachi`, `anh`, `gioitinh`, `dantoc`, `sdt`, `email`, `idtk`) VALUES
(1, 'admin', '2020-04-17', 'admin', 'admin', 'x7Z0O_FB_IMG_1583296631079.jpg', 1, 'admin', '01234567891', 'admin@gmail.com', 1),
(3, 'test', '2020-04-15', 'a', 'a', 'default.png', 1, 'a', '1', 'ab@mvn.com', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `idsv` int(11) NOT NULL,
  `hoten` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ngaysinh` date NOT NULL,
  `gioitinh` tinyint(4) NOT NULL COMMENT '1: Nam | 2: Nữ',
  `dantoc` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quequan` text COLLATE utf8_unicode_ci NOT NULL,
  `diachi` text COLLATE utf8_unicode_ci NOT NULL,
  `cmnd` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sdt` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `anh` text COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default.png',
  `idkhoa` int(11) NOT NULL,
  `idlop` int(11) NOT NULL,
  `idchuyennganh` int(11) NOT NULL,
  `idkhoahoc` int(11) NOT NULL,
  `idloaihinhdaotao` int(11) NOT NULL,
  `idtk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`idsv`, `hoten`, `ngaysinh`, `gioitinh`, `dantoc`, `quequan`, `diachi`, `cmnd`, `email`, `sdt`, `anh`, `idkhoa`, `idlop`, `idchuyennganh`, `idkhoahoc`, `idloaihinhdaotao`, `idtk`) VALUES
(1, 'sv1', '2020-04-10', 1, 'a', 'a', 'sv', '-1', 'ab@mvn.com', '1', 'default.png', 1, 1, 2, 1, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE `taikhoan` (
  `idtk` int(11) NOT NULL,
  `tentk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `matkhau` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `quyen` int(11) NOT NULL COMMENT '1: admin | 2: sv'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`idtk`, `tentk`, `matkhau`, `quyen`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(3, 'sinhvien', '615ad206666f8086103305b8f77173f4', 2),
(4, 'test', 'c4ca4238a0b923820dcc509a6f75849b', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tintuc`
--

CREATE TABLE `tintuc` (
  `idtt` int(11) NOT NULL,
  `tieude` text COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  `idloaitt` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tintuc`
--

INSERT INTO `tintuc` (`idtt`, `tieude`, `noidung`, `idloaitt`, `created_at`, `updated_at`) VALUES
(1, 'Test', '<p>&aacute;dfvbst</p>', 1, '2020-04-05 04:00:59', '2020-04-05 04:00:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chuyennganh`
--
ALTER TABLE `chuyennganh`
  ADD PRIMARY KEY (`idcn`);

--
-- Indexes for table `dichvu`
--
ALTER TABLE `dichvu`
  ADD PRIMARY KEY (`iddv`);

--
-- Indexes for table `dondangky`
--
ALTER TABLE `dondangky`
  ADD PRIMARY KEY (`iddondangky`);

--
-- Indexes for table `khoa`
--
ALTER TABLE `khoa`
  ADD PRIMARY KEY (`idkhoa`);

--
-- Indexes for table `khoahoc`
--
ALTER TABLE `khoahoc`
  ADD PRIMARY KEY (`idkh`);

--
-- Indexes for table `loaidichvu`
--
ALTER TABLE `loaidichvu`
  ADD PRIMARY KEY (`idloaidv`);

--
-- Indexes for table `loaihinhdaotao`
--
ALTER TABLE `loaihinhdaotao`
  ADD PRIMARY KEY (`idlhdt`);

--
-- Indexes for table `loaitintuc`
--
ALTER TABLE `loaitintuc`
  ADD PRIMARY KEY (`idloaitt`);

--
-- Indexes for table `lop`
--
ALTER TABLE `lop`
  ADD PRIMARY KEY (`idlop`);

--
-- Indexes for table `nguoiquantri`
--
ALTER TABLE `nguoiquantri`
  ADD PRIMARY KEY (`idqt`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`idsv`);

--
-- Indexes for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`idtk`);

--
-- Indexes for table `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`idtt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chuyennganh`
--
ALTER TABLE `chuyennganh`
  MODIFY `idcn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `dichvu`
--
ALTER TABLE `dichvu`
  MODIFY `iddv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dondangky`
--
ALTER TABLE `dondangky`
  MODIFY `iddondangky` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `khoa`
--
ALTER TABLE `khoa`
  MODIFY `idkhoa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `khoahoc`
--
ALTER TABLE `khoahoc`
  MODIFY `idkh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `loaidichvu`
--
ALTER TABLE `loaidichvu`
  MODIFY `idloaidv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `loaihinhdaotao`
--
ALTER TABLE `loaihinhdaotao`
  MODIFY `idlhdt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `loaitintuc`
--
ALTER TABLE `loaitintuc`
  MODIFY `idloaitt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lop`
--
ALTER TABLE `lop`
  MODIFY `idlop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nguoiquantri`
--
ALTER TABLE `nguoiquantri`
  MODIFY `idqt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sinhvien`
--
ALTER TABLE `sinhvien`
  MODIFY `idsv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `taikhoan`
--
ALTER TABLE `taikhoan`
  MODIFY `idtk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `idtt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
