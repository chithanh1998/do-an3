<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('dangnhap', 'TaikhoanController@post_dangnhap')->name('dangnhap');
Route::get('dangxuat', 'TaikhoanController@dangxuat')->name('dangxuat');
Route::get('/', 'TaikhoanController@get_dangnhap');
Route::get('thongtincanhan', 'TaikhoanController@get_thongtincanhan')->name('thongtincanhan');
Route::post('thongtincanhan', 'TaikhoanController@post_thongtincanhan')->name('luuthongtincanhan');
Route::post('thongtincanhan/doiavatar', 'TaikhoanController@doiavatar')->name('doiavatar');
Route::get('timkiem', 'TimkiemController@get_tk');
Route::post('timkiem', 'TimkiemController@post_tk');
Route::group(['prefix' => 'nguoiquantri','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'NguoiquantriController@ds')->name('dsqt');
    Route::post('them', 'NguoiquantriController@post_them')->name('themqt');
    Route::get('thongtin', 'NguoiquantriController@thongtin')->name('thongtinqt');
    Route::post('sua', 'NguoiquantriController@post_sua')->name('luuchinhsuaqt');
    Route::get('xoa/{id}', 'NguoiquantriController@xoa');
});
Route::group(['prefix' => 'sinhvien','middleware'=>'dangnhap'], function () {
    Route::get('/', 'SinhvienController@ds')->name('dssv')->middleware('admin');
    Route::post('them', 'SinhvienController@post_them')->name('themsv')->middleware('admin');
    Route::get('thongtin', 'SinhvienController@thongtin')->name('thongtinsv')->middleware('admin');
    Route::post('sua', 'SinhvienController@post_sua')->name('luuchinhsuasv')->middleware('admin');
    Route::get('xoa/{id}', 'SinhvienController@xoa')->middleware('admin');
    Route::get('dondangky', 'SinhvienController@dsddk')->name('dsddksv');
    Route::get('dondangky/xoa/{id}', 'SinhvienController@xoaddk');
    Route::get('loaidichvu', 'SinhvienController@dsloaidv')->name('loaidv');
    Route::get('dichvu/{id_ldv}', 'SinhvienController@dsdv')->name('dv');
    Route::post('dichvu/dkdv', 'SinhvienController@dkdv')->name('dkdv');
    Route::get('tintuc', 'SinhvienController@dstintuc')->name('dstintuc');
    Route::get('tintuc/xem/{id}', 'SinhvienController@xemtintuc')->name('xemtintuc');
});
Route::group(['prefix' => 'chuyennganh','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'ChuyennganhController@ds')->name('dschuyennganh');
    Route::post('them', 'ChuyennganhController@post_them')->name('themchuyennganh');
    Route::get('thongtin', 'ChuyennganhController@thongtin')->name('thongtinchuyennganh');
    Route::post('sua', 'ChuyennganhController@post_sua')->name('luuchinhsuachuyennganh');
    Route::get('xoa/{id}', 'ChuyennganhController@xoa');
});
Route::group(['prefix' => 'khoa','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'KhoaController@ds')->name('dskhoa');
    Route::post('them', 'KhoaController@post_them')->name('themkhoa');
    Route::get('thongtin', 'KhoaController@thongtin')->name('thongtinkhoa');
    Route::post('sua', 'KhoaController@post_sua')->name('luuchinhsuakhoa');
    Route::get('xoa/{id}', 'KhoaController@xoa');
});
Route::group(['prefix' => 'khoahoc','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'KhoahocController@ds')->name('dskhoahoc');
    Route::post('them', 'KhoahocController@post_them')->name('themkhoahoc');
    Route::get('thongtin', 'KhoahocController@thongtin')->name('thongtinkhoahoc');
    Route::post('sua', 'KhoahocController@post_sua')->name('luuchinhsuakhoahoc');
    Route::get('xoa/{id}', 'KhoahocController@xoa');
});
Route::group(['prefix' => 'loaihinhdaotao','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'LoaihinhdaotaoController@ds')->name('dslhdt');
    Route::post('them', 'LoaihinhdaotaoController@post_them')->name('themlhdt');
    Route::get('thongtin', 'LoaihinhdaotaoController@thongtin')->name('thongtinlhdt');
    Route::post('sua', 'LoaihinhdaotaoController@post_sua')->name('luuchinhsualhdt');
    Route::get('xoa/{id}', 'LoaihinhdaotaoController@xoa');
});
Route::group(['prefix' => 'lop','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'LopController@ds')->name('dslop');
    Route::post('them', 'LopController@post_them')->name('themlop');
    Route::get('thongtin', 'LopController@thongtin')->name('thongtinlop');
    Route::post('sua', 'LopController@post_sua')->name('luuchinhsualop');
    Route::get('xoa/{id}', 'LopController@xoa');
});
Route::group(['prefix' => 'tintuc','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'TintucController@ds')->name('dstt');
    Route::get('them', 'TintucController@get_them')->name('get_themtt');
    Route::post('them', 'TintucController@post_them')->name('post_themtt');
    Route::get('sua/{id}', 'TintucController@get_sua');
    Route::post('sua/{id}', 'TintucController@post_sua');
    Route::get('xoa/{id}', 'TintucController@xoa');
    Route::get('loaitt', 'TintucController@dsloaitt')->name('dsloaitt');
    Route::post('loaitt/them', 'TintucController@post_themloaitt')->name('themloaitt');
    Route::get('loaitt/thongtin', 'TintucController@thongtinloaitt')->name('thongtinloaitt');
    Route::post('loaitt/sua', 'TintucController@post_sualoaitt')->name('luuchinhsualoaitt');
    Route::get('loaitt/xoa/{id}', 'TintucController@xoaloaitt');
});
Route::group(['prefix' => 'dichvu','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'DichvuController@ds')->name('dsdv');
    Route::post('them', 'DichvuController@post_them')->name('themdv');
    Route::get('thongtin', 'DichvuController@thongtin')->name('thongtindv');
    Route::post('sua', 'DichvuController@post_sua')->name('luuchinhsuadv');
    Route::get('xoa/{id}', 'DichvuController@xoa');
    Route::get('loaidv', 'DichvuController@dsloaidv')->name('dsloaidv');
    Route::post('loaidv/them', 'DichvuController@post_themloaidv')->name('themloaidv');
    Route::get('loaidv/thongtin', 'DichvuController@thongtinloaidv')->name('thongtinloaidv');
    Route::post('loaidv/sua', 'DichvuController@post_sualoaidv')->name('luuchinhsualoaidv');
    Route::get('loaidv/xoa/{id}', 'DichvuController@xoaloaidv');
});
Route::group(['prefix' => 'dondangky','middleware'=>['dangnhap','admin']], function () {
    Route::get('/', 'DondangkyController@ds')->name('dsdondangkyadmin');
    Route::get('dangxuly/{id}', 'DondangkyController@dangxuly');
    Route::get('hoanthanh/{id}', 'DondangkyController@hoanthanh');
    Route::get('thongtin', 'DondangkyController@thongtin')->name('thongtinddk');
});