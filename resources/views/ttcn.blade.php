@extends('layout.main')
@section('css')
<link href="plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                    <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Thông tin cá nhân</h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-4">
                                            <div class="text-center card-box">
                                                <div class="member-card">
                                                    <div class="thumb-xl member-thumb m-b-10 center-block">
                                                        <img src="upload/avatar/{{$tk->anh}}" class="img-circle img-thumbnail" alt="profile-image">
                                                    </div>

                                                    <div class="">
                                                        <h4 class="m-b-5">{{$tk->hoten}}</h4>
                                                    </div>
                                            <form action="{{route('doiavatar')}}" id="formanh" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input name="anh" type="file" id="file" style="display:none;" accept="image/*"/>
                                            <button type="button" id="upload" class="btn btn-success btn-sm w-sm waves-effect m-t-10 waves-light">Đổi avatar</button>
                                            </form>
                                                    <hr>

                                                    <div class="text-left">
                                                        <p class="text-muted font-13"><strong>Họ tên :</strong> <span class="m-l-15">{{$tk->hoten}}</span></p>

                                                        <p class="text-muted font-13"><strong>SDT :</strong><span class="m-l-15">{{$tk->sdt}}</span></p>

                                                        <p class="text-muted font-13"><strong>Email :</strong> <span class="m-l-15">{{$tk->email}}</span></p>

                                                    </div>

                                                </div>

                                            </div> <!-- end card-box -->

                                        </div> <!-- end col -->

                                        <div class="col-md-8 col-lg-9">
                                        <form class="form-horizontal" method="POST" action="{{route('luuthongtincanhan')}}">
                                        @csrf
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Họ tên</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="{{$tk->hoten}}" name="hoten" required>
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label" for="example-email">Email</label>
	                                                <div class="col-md-10">
	                                                    <input type="email" id="example-email" name="email" value="{{$tk->email}}" class="form-control" placeholder="" required>
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Ngày sinh</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" name="ngaysinh" id="ngaysinh" value="{{$tk->ngaysinh}}" readonly>
	                                                </div>
	                                            </div>

	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Dân tộc</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="{{$tk->dantoc}}" placeholder="" name="dantoc" required>
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="col-md-2 control-label">Quê quán</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="{{$tk->quequan}}" placeholder="" name="quequan" required>
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="col-md-2 control-label">Địa chỉ</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="{{$tk->diachi}}" placeholder="" name="diachi" required>
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Số điện thoại</label>
	                                                <div class="col-md-10">
	                                                    <input type="number" class="form-control" value="{{$tk->sdt}}" placeholder="" name="sdt" required>
	                                                </div>
	                                            </div>
                                                <div class="form-group">
	                                                <label class="col-sm-2 control-label">Giới tính</label>
	                                                <div class="col-sm-10">
	                                                    <select class="form-control" name="gioitinh" value="{{$tk->gioitinh}}">
	                                                        <option value="1">Nam</option>
	                                                        <option value="2">Nữ</option>
	                                                    </select>
	                                                </div>
	                                            </div>
                                                @if(session('quyen') == 'sv')
                                                <div class="form-group">
	                                                <label class="col-md-2 control-label">CMND</label>
	                                                <div class="col-md-10">
	                                                    <input type="number" class="form-control" placeholder="" name="cmnd" value="{{$tk->cmnd}}" required>
	                                                </div>
	                                            </div>
                                                @endif
                                                <div class="col-sm-offset-3 col-sm-9">
	                                                  <button type="submit" class="btn btn-info waves-effect waves-light">Lưu</button>
	                                                </div>

	                                        </form>

                                            <hr>

                                        </div>
                                        <!-- end col -->

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>

            
                                

@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif

$(document).ready(function () {
    $('#upload').click(function(){
        $('#file').trigger('click');
    });
    $("#file").change(function () {
        $("#formanh").submit();
    });
    jQuery('#ngaysinh').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startView: 2,
    });
            });
        </script>
@endsection