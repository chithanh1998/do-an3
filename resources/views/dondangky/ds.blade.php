@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Đơn đăng ký </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                    <h4 class="m-t-5 header-title"><b>Danh sách đơn đăng ký</b></h4>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Tên sinh viên</th>
                                            <th>Dịch vụ</th>
                                            <th>Trạng thái</th>
                                            <th>Thời gian tạo</th>
                                            <th>Quản lý</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                            @foreach($ddk as $d)
                                        <tr>
                                            <td>{{$d->hoten}}</td>
                                            <td>{{$d->tendv}}</td>
                                            @if($d->trangthai == 0)
                                            <td>Đang chờ</td>
                                            @elseif($d->trangthai == 1)
                                            <td>Đang xử lý</td>
                                            @else
                                            <td>Hoàn thành</td>
                                            @endif
                                            <td>{{$d->created_at}}</td>
                                            <td><a data-toggle="modal" id="{{$d->iddondangky}}" data-target="#con-close-modal" class="xemchitiet" title="Xem chi tiết"><i class="fa fa-search"></i></a>
                                            @if($d->trangthai == 0)
                                            <a href="dondangky/dangxuly/{{$d->iddondangky}}" title="Chấp nhận"><i class="fa fa-check"></i></a>
                                            @elseif($d->trangthai == 1)
                                            <a href="dondangky/hoanthanh/{{$d->iddondangky}}" title="Hoàn thành"><i class="fa fa-check"></i></a>
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>

            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Chi tiết đơn đăng ký</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Dịch vụ</label>
                                                                <select class="form-control" id="dichvu" readonly>
                                                                @foreach($dv as $d)
	                                                                <option value="{{$d->iddv}}">{{$d->tendv}}</option>
                                                                @endforeach
                                                                </select>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Lý do làm đơn</label>
                                                                <input type="text" class="form-control" id="ldld" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Họ tên</label>
                                                                <input type="text" class="form-control" id="hoten" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Địa chỉ</label>
                                                                <input type="text" class="form-control" id="diachi" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Ngày sinh</label>
                                                                <input type="text" class="form-control" id="ngaysinh" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-5" class="control-label">Quê Quán</label>
                                                                <input type="text" class="form-control" id="quequan" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-6" class="control-label">Dân tộc</label>
                                                                <input type="text" class="form-control" id="dantoc" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Email</label>
                                                                <input type="text" class="form-control" id="email" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số điện thoại</label>
                                                                <input type="number" class="form-control" id="sdt" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số CMND</label>
                                                                <input type="number" class="form-control" id="cmnd" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Giới tính</label>
                                                                <select class="form-control" id="gioitinh" readonly>
	                                                                <option value="1">Nam</option>
	                                                                <option value="2">Nữ</option>
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Lớp</label>
                                                                <select class="form-control" id="lop" readonly>
                                                                @foreach($lop as $l)
	                                                                <option value="{{$l->idlop}}">{{$l->tenlop}}</option>
                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khoa</label>
                                                                <select class="form-control" id="khoa" readonly>
                                                                @foreach($khoa as $k)
	                                                                <option value="{{$k->idkhoa}}">{{$k->tenkhoa}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Chuyên ngành</label>
                                                                <select class="form-control" id="chuyennganh" readonly>
                                                                @foreach($chuyennganh as $cn)
	                                                                <option value="{{$cn->idcn}}">{{$cn->tenchuyennganh}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khóa học</label>
                                                                <select class="form-control" id="khoahoc" readonly>
                                                                @foreach($khoahoc as $kh)
	                                                                <option value="{{$kh->idkh}}">{{$kh->nambatdau}} - {{$kh->namketthuc}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Loại hình đào tạo</label>
                                                                <select class="form-control" id="lhdt" readonly>
                                                                @foreach($loaihinhdaotao as $lhdt)
	                                                                <option value="{{$lhdt->idlhdt}}">{{$lhdt->tenlhdt}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                

@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="plugins/select2/js/select2.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
$(document).ready(function () {
    $('#datatable').dataTable();
    $("#datatable").on("click", ".xemchitiet", function(){
        $('#preloader').fadeIn();
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: '{{route("thongtinddk")}}',
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                console.log(resp.hoten);
                $('#preloader').fadeOut();
                $('#hoten').val(resp.hoten);
                $('#diachi').val(resp.diachi);
                $('#ngaysinh').val(resp.ngaysinh);
                $('#email').val(resp.email);
                $('#sdt').val(resp.sdt);
                $('#quequan').val(resp.quequan);
                $('#gioitinh').val(resp.gioitinh);
                $('#dantoc').val(resp.dantoc);
                $('#cmnd').val(resp.cmnd);
                $('#lop').val(resp.idlop);
                $('#chuyennganh').val(resp.idchuyennganh);
                $('#khoahoc').val(resp.idkhoahoc);
                $('#khoa').val(resp.idkhoa);
                $('#lhdt').val(resp.idloaihinhdaotao);
                $('#dichvu').val(resp.iddv);
                $('#ldld').val(resp.lydolamdon);
            }
        })
    })
            });
        </script>
@endsection