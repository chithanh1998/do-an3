<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <base href="{{asset('')}}">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App title -->
        <title>Đăng nhập</title>

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body class="bg-transparent">
        <!-- Loader -->
        <div id="preloader" style="display: none">
                    <div id="status">
                        <div class="spinner">
                          <div class="spinner-wrapper">
                            <div class="rotator">
                              <div class="inner-spin"></div>
                              <div class="inner-spin"></div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>

        <!-- HOME -->
        <section>
            <div class="container-alt">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="m-t-40 account-pages">
                                <div class="text-center account-logo-box">
                                    <h2 class="text-uppercase" style="color:white">
                                    HÀNH CHÍNH MỘT CỬA
                                        <!-- <a href="index.html" class="text-success">
                                            <span><img src="assets/images/logo.png" alt="" height="36"></span>
                                        </a> -->
                                    </h2>
                                    <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
                                </div>
                                <div class="account-content">
                                    <form class="form-horizontal">

                                        <div class="form-group ">
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" required="" placeholder="Tên tài khoản" id="tentaikhoan">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <input class="form-control" type="password" required="" placeholder="Mật khẩu" id="matkhau">
                                            </div>
                                        </div>

                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-xs-12">
                                                <button class="btn w-md btn-bordered btn-danger waves-effect waves-light" type="button" id="dangnhap">Đăng nhập</button>
                                            </div>
                                        </div>

                                    </form>

                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <!-- end card-box-->


                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
          </section>
          <!-- END HOME -->

        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>
        <script src="plugins/toastr/toastr.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>
    <script>
        @if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
            // Enter đăng nhập
        $(document).bind('keydown', function(e){         
    if (e.which == 13){
       $('#dangnhap').trigger('click');   
    }     
        });
        $('#dangnhap').click(function(){
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tentaikhoan = $('#tentaikhoan').val();
        var matkhau = $('#matkhau').val();
        $.ajax({
            type: 'post',
            url: '{{route("dangnhap")}}',
            data: {tentaikhoan:tentaikhoan, matkhau:matkhau},
            beforeSend: function(){
                if(tentaikhoan == "" || matkhau == ""){
                    toastr["info"]("Hãy nhập tên tài khoản và mật khẩu");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                if(resp == "ok"){
                    setTimeout('window.location.href = "{{route("dstintuc")}}";',1000);
                }
				if(resp == "admin"){
                    setTimeout('window.location.href = "{{route("dsqt")}}";',1000);
                }
                if(resp == "err"){
                    $('#preloader').fadeOut();
                    toastr["info"]("Tài khoản và mật khẩu không đúng");
                }
            } 
        })
        })
    </script>
</html>