@if(session('quyen') == 'admin')
<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                        	<li class="menu-title">Quản trị</li>

                            <li>
                                <a href="{{route('dsqt')}}" class="waves-effect"><i class="mdi mdi-account"></i><span> Người quản trị </span></a>
                            </li>
                            <li>
                                <a href="timkiem" class="waves-effect"><i class="mdi mdi-account-search"></i><span> Tìm kiếm sinh viên </span></a>
                            </li>
                            <li class="menu-title">Đào tạo</li>
                            <li>
                                <a href="{{route('dsdondangkyadmin')}}" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i><span> Đơn đăng ký </span></a>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-layers"></i><span> Dịch vụ </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="{{route('dsdv')}}">Dịch vụ</a></li>
                                    <li><a href="{{route('dsloaidv')}}">Loại dịch vụ</a></li>
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-newspaper"></i><span> Tin tức </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="{{route('dstt')}}">Tin tức</a></li>
                                    <li><a href="{{route('dsloaitt')}}">Loại tin tức</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{route('dssv')}}" class="waves-effect"><i class="mdi mdi-account-card-details"></i><span> Sinh viên </span></a>
                            </li>
                            <li>
                                <a href="{{route('dslop')}}" class="waves-effect"><i class="mdi mdi-animation"></i><span> Lớp </span></a>
                            </li>
                            <li>
                                <a href="{{route('dskhoa')}}" class="waves-effect"><i class="mdi mdi-code-greater-than"></i><span> Khoa </span></a>
                            </li>
                            <li>
                                <a href="{{route('dschuyennganh')}}" class="waves-effect"><i class="mdi mdi-code-greater-than"></i><span> Chuyên ngành </span></a>
                            </li>
                            <li>
                                <a href="{{route('dskhoahoc')}}" class="waves-effect"><i class="mdi mdi-code-greater-than"></i><span> Khóa học </span></a>
                            </li>
                            <li>
                                <a href="{{route('dslhdt')}}" class="waves-effect"><i class="mdi mdi-code-greater-than"></i><span> Loại hình đào tạo </span></a>
                            </li>
                        

                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
@else
<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                            <li>
                                <a href="{{route('dsddksv')}}" class="waves-effect"><i class="mdi mdi-format-list-bulleted"></i><span> Đơn đăng ký </span></a>
                            </li>
                            <li>
                                <a href="{{route('loaidv')}}" class="waves-effect"><i class="mdi mdi-layers"></i><span> Dịch vụ </span></a>
                            </li>
                            <li>
                                <a href="{{route('dstintuc')}}" class="waves-effect"><i class="mdi mdi-newspaper"></i><span> Tin tức </span></a>
                            </li>
                    
                        

                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            @endif