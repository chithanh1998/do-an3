@extends('layout.main')
@section('css')
<link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Chuyên ngành </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                <button class="btn btn-info waves-effect waves-light m-b-5" style="float: right" data-toggle="modal" data-target="#con-close-modal"> <i class="fa fa-plus m-r-5"></i> <span>Thêm</span> </button>
                                    <h4 class="m-t-5 header-title"><b>Danh sách chuyên ngành</b></h4>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Tên chuyên ngành</th>
                                            <th>Khoa</th>
                                            <th>Quản lý</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        @foreach($chuyennganh as $cn)
                                        <tr>
                                            <td>{{$cn->tenchuyennganh}}</td>
                                            <td>{{$cn->tenkhoa}}</td>
                                            <td><a data-toggle="modal" id="{{$cn->idcn}}" data-target="#con-close-modal2" class="chinhsua"><i class="fa fa-pencil"></i></a>
                                            <a href="chuyennganh/xoa/{{$cn->idcn}}" onclick="javascript:confirmationDelete($(this));return false;"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Thêm chuyên ngành</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tên chuyên ngành</label>
                                                                <input type="text" class="form-control" id="tenchuyennganh" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khoa</label>
                                                                <select class="form-control select2" id="khoa">
                                                                @foreach($khoa as $k)
	                                                                <option value="{{$k->idkhoa}}">{{$k->tenkhoa}}</option>
	                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="add">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->


                                    <div id="con-close-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Chỉnh sửa chuyên ngành</h4>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tên chuyên ngành</label>
                                                                <input type="text" class="form-control" id="tenchuyennganh_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khoa</label>
                                                                <select class="form-control select2" id="khoa_edit">
	                                                            @foreach($khoa as $k)
	                                                                <option value="{{$k->idkhoa}}">{{$k->tenkhoa}}</option>
	                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <input type="hidden" id="id_chuyennganh" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="save">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="plugins/select2/js/select2.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();
    $('.select2').each(function () {
                $(this).select2({
                    dropdownParent: $(this).parent()
                });
            });
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tenchuyennganh = $('#tenchuyennganh').val();
        var khoa = $('#khoa').val();
        $.ajax({
            type: 'post',
            url: '{{route("themchuyennganh")}}',
            data: {
                tenchuyennganh: tenchuyennganh, khoa: khoa
            },
            beforeSend: function(){ 
                if(tenchuyennganh == ""){
                    toastr["info"]("Hãy nhập tên chuyên ngành");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){            
                    toastr["success"]("Thêm thành công.");
                    setTimeout('window.location.href = "{{route("dschuyennganh")}}";',1500);

                } else {
                    toastr["info"]("Tên chuyên ngành đã tồn tại");
                }
            }
        })
    })
    $("#datatable").on("click", ".chinhsua", function(){
        $('#preloader').fadeIn();
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: '{{route("thongtinchuyennganh")}}',
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('#preloader').fadeOut();
                $('#tenchuyennganh_edit').val(resp.tenchuyennganh);
                $('#khoa_edit').val(resp.idkhoa).trigger('change');
                $('#id_chuyennganh').val(resp.idcn);
            }
        })
    })
    // Lưu thông tin chỉnh sửa
    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var idchuyennganh = $('#id_chuyennganh').val();
        var tenchuyennganh = $('#tenchuyennganh_edit').val();
        var khoa = $('#khoa_edit').val();
        $.ajax({
            type: 'post',
            url: '{{route("luuchinhsuachuyennganh")}}',
            data: {
                idchuyennganh: idchuyennganh, tenchuyennganh: tenchuyennganh, khoa: khoa
            },
            beforeSend: function(){
                if(tenchuyennganh == ""){
                    toastr["info"]("Hãy nhập tên chuyên ngành");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){
                    toastr["success"]("Lưu thành công.");
                    setTimeout('window.location.href = "{{route("dschuyennganh")}}";',1500);
                } else {
                    toastr["info"]("Tên chuyên ngành đã tồn tại");
                }
            }
        })
    })
            });
        </script>
@endsection