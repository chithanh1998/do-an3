@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Loại hình đào tạo </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                <button class="btn btn-info waves-effect waves-light m-b-5" style="float: right" data-toggle="modal" data-target="#con-close-modal"> <i class="fa fa-plus m-r-5"></i> <span>Thêm</span> </button>
                                    <h4 class="m-t-5 header-title"><b>Danh sách loại hình đào tạo</b></h4>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Loại hình đào tạo</th>
                                            <th>Quản lý</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        @foreach($lhdt as $dt)
                                        <tr>
                                            <td>{{$dt->tenlhdt}}</td>
                                            <td><a data-toggle="modal" id="{{$dt->idlhdt}}" data-target="#con-close-modal2" class="chinhsua"><i class="fa fa-pencil"></i></a>
                                            <a href="loaihinhdaotao/xoa/{{$dt->idlhdt}}" onclick="javascript:confirmationDelete($(this));return false;"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Thêm loại hình đào tạo</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tên loại hình đào tạo</label>
                                                                <input type="text" class="form-control" id="lhdt" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="add">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->


                                    <div id="con-close-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Chỉnh sửa loại hình đào tạo</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tên loại hình đào tạo</label>
                                                                <input type="text" class="form-control" id="lhdt_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <input type="hidden" id="id_lhdt" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="save">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var lhdt = $('#lhdt').val();
        $.ajax({
            type: 'post',
            url: '{{route("themlhdt")}}',
            data: {
                lhdt: lhdt
            },
            beforeSend: function(){ 
                if(lhdt == ""){
                    toastr["info"]("Hãy nhập tên loại hình đào tạo");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){            
                    toastr["success"]("Thêm thành công.");
                    setTimeout('window.location.href = "{{route("dslhdt")}}";',1500);

                } else {
                    toastr["info"]("Tên loại hình đào tạo đã tồn tại");
                }
            }
        })
    })
    $("#datatable").on("click", ".chinhsua", function(){
        $('#preloader').fadeIn();
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: '{{route("thongtinlhdt")}}',
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('#preloader').fadeOut();
                $('#lhdt_edit').val(resp.tenlhdt);
                $('#id_lhdt').val(resp.idlhdt);
            }
        })
    })
    // Lưu thông tin chỉnh sửa
    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var idlhdt = $('#id_lhdt').val();
        var lhdt = $('#lhdt_edit').val();
        $.ajax({
            type: 'post',
            url: '{{route("luuchinhsualhdt")}}',
            data: {
                idlhdt: idlhdt, lhdt: lhdt
            },
            beforeSend: function(){
                if(lhdt == ""){
                    toastr["info"]("Hãy nhập tên loại hình đào tạo");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){
                    toastr["success"]("Lưu thành công.");
                    setTimeout('window.location.href = "{{route("dslhdt")}}";',1500);
                } else {
                    toastr["info"]("Tên loại hình đào tạo đã tồn tại");
                }
            }
        })
    })
            });
        </script>
@endsection