@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Khóa học </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                <button class="btn btn-info waves-effect waves-light m-b-5" style="float: right" data-toggle="modal" data-target="#con-close-modal"> <i class="fa fa-plus m-r-5"></i> <span>Thêm</span> </button>
                                    <h4 class="m-t-5 header-title"><b>Danh sách khóa hoc</b></h4>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Khóa học</th>
                                            <th>Quản lý</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                            @foreach($khoahoc as $kh)
                                        <tr>
                                            <td>{{$kh->nambatdau}} - {{$kh->namketthuc}}</td>
                                            <td><a data-toggle="modal" id="{{$kh->idkh}}" data-target="#con-close-modal2" class="chinhsua"><i class="fa fa-pencil"></i></a>
                                            <a href="khoahoc/xoa/{{$kh->idkh}}" onclick="javascript:confirmationDelete($(this));return false;"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Thêm khóa học</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Năm bắt đầu</label>
                                                                <input type="number" class="form-control" id="nambatdau" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Năm kết thúc</label>
                                                                <input type="number" class="form-control" id="namketthuc" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="add">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->


                                    <div id="con-close-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Chỉnh sửa khóa học</h4>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">
                                                <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Năm bắt đầu</label>
                                                                <input type="number" class="form-control" id="nambatdau_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Năm kết thúc</label>
                                                                <input type="number" class="form-control" id="namketthuc_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <input type="hidden" id="id_khoahoc" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="save">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var nambatdau = $('#nambatdau').val();
        var namketthuc = $('#namketthuc').val();
        $.ajax({
            type: 'post',
            url: '{{route("themkhoahoc")}}',
            data: {
                nambatdau: nambatdau, namketthuc: namketthuc
            },
            beforeSend: function(){ 
                if(nambatdau == "" || namketthuc == ""){
                    toastr["info"]("Hãy nhập tất cả các trường");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){            
                    toastr["success"]("Thêm thành công.");
                    setTimeout('window.location.href = "{{route("dskhoahoc")}}";',1500);

                } else {
                    toastr["info"]("Khóa học đã tồn tại");
                }
            }
        })
    });
    $("#datatable").on("click", ".chinhsua", function(){
        $('#preloader').fadeIn();
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: '{{route("thongtinkhoahoc")}}',
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('#preloader').fadeOut();
                $('#nambatdau_edit').val(resp.nambatdau);
                $('#namketthuc_edit').val(resp.namketthuc);
                $('#id_khoahoc').val(resp.idkhoahoc);
            }
        })
    });
    // Lưu thông tin chỉnh sửa
    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var idkhoahoc = $('#id_khoahoc').val();
        var nambatdau = $('#nambatdau_edit').val();
        var namketthuc = $('#namketthuc_edit').val();
        $.ajax({
            type: 'post',
            url: '{{route("luuchinhsuakhoahoc")}}',
            data: {
                idkhoahoc: idkhoahoc, nambatdau: nambatdau, namketthuc: namketthuc
            },
            beforeSend: function(){ 
                if(nambatdau == "" || namketthuc ==""){
                    toastr["info"]("Hãy nhập tất cả các trường");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){
                    toastr["success"]("Lưu thành công.");
                    setTimeout('window.location.href = "{{route("dskhoahoc")}}";',1500);
                } else {
                    toastr["info"]("Khóa học đã tồn tại");
                }
            }
        })
    })
            });
        </script>
@endsection