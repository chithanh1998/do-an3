@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Tin tức </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                    <h4 class="m-t-5 header-title"><b>Thêm tin tức</b></h4>
                                    </div>
                                    <div class="col-md-12">
                        					<form class="form-horizontal" method="POST" action="tintuc/them">
											@csrf
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Tiêu đề</label>
	                                                <div class="col-md-10">
	                                                    <input type="text" class="form-control" value="" name="tieude">
	                                                </div>
	                                            </div>
												<div class="form-group">
	                                                <label class="col-md-2 control-label">Loại tin tức</label>
	                                                <div class="col-md-10">
													<select class="form-control select2" name="loaitt">
                                                                    @foreach($loaitt as $l)
	                                                                <option value="{{$l->idloaitt}}">{{$l->tenloaitt}}</option>
	                                                                @endforeach
                                                                </select>
	                                                </div>
	                                            </div>
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">Nội dung</label>
	                                                <div class="col-md-10">
	                                                    <textarea class="form-control" id="noidung" name="noidung"></textarea>
	                                                </div>
	                                            </div>
                                                <button type="submit" class="btn btn-info waves-effect waves-light m-t-5"> <i class="fa fa-save m-r-5"></i> <span>Lưu</span> </button>
	                                        </form>
                        				</div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


@endsection
@section('js')
<!--Wysiwig js-->
<script src="plugins/tinymce/tinymce.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
$(document).ready(function () {
    if($("#noidung").length > 0){
			        tinymce.init({
			            selector: "textarea#noidung",
			            theme: "modern",
			            height:300,
			            plugins: [
			                "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
			                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
			                "save table contextmenu directionality emoticons template paste textcolor"
			            ],
			            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview fullpage | forecolor backcolor emoticons",
			            style_formats: [
			                {title: 'Bold text', inline: 'b'},
			                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			                {title: 'Header', block: 'h1',},
			                {title: 'Example 1', inline: 'span', classes: 'example1'},
			                {title: 'Example 2', inline: 'span', classes: 'example2'},
			                {title: 'Table styles'},
			                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
			            ]
			        });
			    }
});
        </script>
@endsection