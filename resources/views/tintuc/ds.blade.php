@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Tin tức </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                <a href="tintuc/them" class="btn btn-info waves-effect waves-light m-b-5" style="float: right"> <i class="fa fa-plus m-r-5"></i> <span>Thêm</span> </a>
                                    <h4 class="m-t-5 header-title"><b>Danh sách tin tức</b></h4>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Tiêu đề</th>
                                            <th>Ngày tạo</th>
                                            <th>Quản lý</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        @foreach($tintuc as $t)
                                        <tr>
                                            <td>{{$t->tieude}}</td>
                                            <td>{{$t->created_at}}</td>
                                            <td><a href="tintuc/sua/{{$t->idtt}}"><i class="fa fa-pencil"></i></a>
                                            <a href="tintuc/xoa/{{$t->idtt}}" onclick="javascript:confirmationDelete($(this));return false;"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


@endsection
@section('js')
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();
            });
        </script>
@endsection