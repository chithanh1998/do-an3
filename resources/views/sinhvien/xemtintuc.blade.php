@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                    <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">{{$tintuc->tieude}}</h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>

                        <div class="row">
							<div class="col-md-12">
								<div class="card-box">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            {!!$tintuc->noidung!!}
                                        </div>

                                    </div>

                                </div>
							</div>

						</div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>

            
                                

@endsection
@section('js')
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
$(document).ready(function () {
            });
        </script>
@endsection