@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Chọn dịch vụ</h4>

                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;"><div class="inbox-widget slimscroll-alt" style="min-height: 302px; overflow: hidden; width: auto; height: 250px;">
                                        @foreach($dv as $d)
                                        <a data-toggle="modal" id="{{$d->iddv}}" name="{{$d->tendv}}" class="dangky" data-target="#con-close-modal2" style="cursor: pointer;">
                                            <div class="inbox-item">
                                            <div class="inbox-item-img"><i class="mdi mdi-arrow-right"></i></div>
                                                <p class="inbox-item-author">{{$d->tendv}}</p>
                                            </div>
                                        </a>
                                        @endforeach
                                    </div><div class="slimScrollBar" style="background: rgb(152, 166, 173); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 183.879px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                                </div> <!-- end card -->
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>

            <div id="con-close-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog" style="width:70%;">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Đăng ký dịch vụ</h4>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-11">
                                                    <p style="color:red; text-align:right; margin:15px; margin-right:25px">*Sinh viên bắt buộc phải xem mẫu đơn trước khi đăng ký đơn</p>
                                                </div>
                                                <div class="col-md-1">
                                                    <button style="float:right; margin:10px" type="button" class="btn btn-default waves-effect w-md m-b-5">Đơn mẫu</button>
                                                </div>
                                                </div>
                                                <div class="row">
                                                    <h4 class="col-md-6" style="text-align: center">BỘ GIAO THÔNG VẬN TẢI</h4>
                                                    <h4 class="col-md-6" style="text-align: center">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h4>
                                                </div>
                                                <div class="row">
                                                    <u class="col-md-6" style="text-align: center">TRƯỜNG ĐẠI HỌC CÔNG NGHỆ GIAO THÔNG VẬN TẢI</u>
                                                    <u class="col-md-6" style="text-align: center">Độc lập - Tự do - Hạnh phúc</u>
                                                </div>
                                                <div class="row">
                                                    <h4 class="col-md-12" style="text-align: center; margin-top: 20px; margin-bottom: 30px;" id="tendv"></h4>
                                                </div>
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Họ tên</label>
                                                                <input type="text" class="form-control" id="hoten" placeholder="" readonly value="{{$sv->hoten}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Giới tính</label>
                                                                <select class="form-control" id="gioitinh" readonly value="{{$sv->gioitinh}}">
	                                                                <option value="1">Nam</option>
	                                                                <option value="2">Nữ</option>
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Ngày sinh</label>
                                                                <input type="text" class="form-control" id="ngaysinh" placeholder="" readonly value="{{$sv->ngaysinh}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-6" class="control-label">Dân tộc</label>
                                                                <input type="text" class="form-control" id="dantoc" placeholder="" readonly value="{{$sv->dantoc}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số CMND</label>
                                                                <input type="number" class="form-control" id="cmnd" placeholder="" readonly value="{{$sv->cmnd}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số điện thoại</label>
                                                                <input type="number" class="form-control" id="sdt" placeholder="" readonly value="{{$sv->sdt}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Email</label>
                                                                <input type="text" class="form-control" id="email" placeholder="" readonly value="{{$sv->email}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-5" class="control-label">Quê Quán</label>
                                                                <input type="text" class="form-control" id="quequan" placeholder="" readonly value="{{$sv->quequan}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Địa chỉ</label>
                                                                <input type="text" class="form-control" id="diachi" placeholder="" readonly value="{{$sv->diachi}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khoa</label>
                                                                <select class="form-control" id="khoa" readonly value="{{$sv->idkhoa}}">
                                                                @foreach($khoa as $k)
	                                                                <option value="{{$k->idkhoa}}">{{$k->tenkhoa}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Lớp</label>
                                                                <select class="form-control" id="lop" readonly value="{{$sv->idlop}}">
                                                                @foreach($lop as $l)
	                                                                <option value="{{$l->idlop}}">{{$l->tenlop}}</option>
                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Chuyên ngành</label>
                                                                <select class="form-control" id="chuyennganh" readonly value="{{$sv->idchuyennganh}}">
                                                                @foreach($chuyennganh as $cn)
	                                                                <option value="{{$cn->idcn}}">{{$cn->tenchuyennganh}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khóa học</label>
                                                                <select class="form-control" id="khoahoc" readonly value="{{$sv->idkhoahoc}}">
                                                                @foreach($khoahoc as $kh)
	                                                                <option value="{{$kh->idkh}}">{{$kh->nambatdau}} - {{$kh->namketthuc}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Loại hình đào tạo</label>
                                                                <select class="form-control" id="lhdt" readonly value="{{$sv->idloaihinhdaotao}}">
                                                                @foreach($loaihinhdaotao as $lhdt)
	                                                                <option value="{{$lhdt->idlhdt}}">{{$lhdt->tenlhdt}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Lý do làm đơn</label>
                                                                <textarea class="form-control autogrow" id="lydo" placeholder="" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 104px;"></textarea>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="add">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="plugins/select2/js/select2.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();
    $('.dangky').click(function(){
        var id = $(this).attr("id");
        var tendv = $(this).attr("name");
        $('#tendv').html(tendv.toUpperCase());
        $('#add').click(function(){
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
            var lydo = $('#lydo').val();
            $.ajax({
            type: 'post',
            url: '{{route("dkdv")}}',
            data: {
                iddv: id, lydo: lydo
            },
            beforeSend: function(){ 
                if(lydo == ""){
                    toastr["info"]("Hãy nhập lý do làm đơn");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){            
                    toastr["success"]("Thêm thành công.");
                } else {
                }
            }
        })
        })
    })
});
        </script>
@endsection