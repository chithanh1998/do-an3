@extends('layout.main')
@section('css')
<link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<link href="plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Sinh viên </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                <button class="btn btn-info waves-effect waves-light m-b-5" style="float: right" data-toggle="modal" data-target="#con-close-modal"> <i class="fa fa-plus m-r-5"></i> <span>Thêm</span> </button>
                                    <h4 class="m-t-5 header-title"><b>Danh sách sinh viên</b></h4>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Ngày sinh</th>
                                            <th>Lớp</th>
                                            <th>Khoa</th>
                                            <th>Khóa học</th>
                                            <th>LHĐT</th>
                                            <th>Quản lý</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        @foreach($sv as $s)
                                        <tr>
                                            <td>{{$s->hoten}}</td>
                                            <td>{{$s->ngaysinh}}</td>
                                            <td>{{$s->tenlop}}</td>
                                            <td>{{$s->tenkhoa}}</td>
                                            <td>{{$s->nambatdau}} - {{$s->namketthuc}}</td>
                                            <td>{{$s->tenlhdt}}</td>
                                            <td><a data-toggle="modal" id="{{$s->idsv}}" data-target="#con-close-modal2" class="chinhsua"><i class="fa fa-pencil"></i></a>
                                            <a href="sinhvien/xoa/{{$s->idsv}}" onclick="javascript:confirmationDelete($(this));return false;"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Thêm sinh viên</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tên tài khoản</label>
                                                                <input type="text" class="form-control" id="tentk" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Mật khẩu</label>
                                                                <input type="password" class="form-control" id="matkhau" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Họ tên</label>
                                                                <input type="text" class="form-control" id="hoten" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Địa chỉ</label>
                                                                <input type="text" class="form-control" id="diachi" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Ngày sinh</label>
                                                                <input type="text" class="form-control" id="ngaysinh" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-5" class="control-label">Quê Quán</label>
                                                                <input type="text" class="form-control" id="quequan" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-6" class="control-label">Dân tộc</label>
                                                                <input type="text" class="form-control" id="dantoc" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Email</label>
                                                                <input type="text" class="form-control" id="email" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số điện thoại</label>
                                                                <input type="number" class="form-control" id="sdt" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số CMND</label>
                                                                <input type="number" class="form-control" id="cmnd" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Giới tính</label>
                                                                <select class="form-control" id="gioitinh">
	                                                                <option value="1">Nam</option>
	                                                                <option value="2">Nữ</option>
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Lớp</label>
                                                                <select class="form-control select2" id="lop">
                                                                @foreach($lop as $l)
	                                                                <option value="{{$l->idlop}}">{{$l->tenlop}}</option>
                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khoa</label>
                                                                <select class="form-control select2" id="khoa">
                                                                @foreach($khoa as $k)
	                                                                <option value="{{$k->idkhoa}}">{{$k->tenkhoa}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Chuyên ngành</label>
                                                                <select class="form-control select2" id="chuyennganh">
                                                                @foreach($chuyennganh as $cn)
	                                                                <option value="{{$cn->idcn}}">{{$cn->tenchuyennganh}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khóa học</label>
                                                                <select class="form-control select2" id="khoahoc">
                                                                @foreach($khoahoc as $kh)
	                                                                <option value="{{$kh->idkh}}">{{$kh->nambatdau}} - {{$kh->namketthuc}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Loại hình đào tạo</label>
                                                                <select class="form-control select2" id="lhdt">
                                                                @foreach($loaihinhdaotao as $lhdt)
	                                                                <option value="{{$lhdt->idlhdt}}">{{$lhdt->tenlhdt}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="add">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->


                                    <div id="con-close-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Chỉnh sửa sinh viên</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Họ tên</label>
                                                                <input type="text" class="form-control" id="hoten_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Địa chỉ</label>
                                                                <input type="text" class="form-control" id="diachi_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Ngày sinh</label>
                                                                <input type="text" class="form-control" id="ngaysinh_edit" placeholder="" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-5" class="control-label">Quê Quán</label>
                                                                <input type="text" class="form-control" id="quequan_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-6" class="control-label">Dân tộc</label>
                                                                <input type="text" class="form-control" id="dantoc_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Email</label>
                                                                <input type="text" class="form-control" id="email_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số điện thoại</label>
                                                                <input type="number" class="form-control" id="sdt_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Số CMND</label>
                                                                <input type="number" class="form-control" id="cmnd_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Giới tính</label>
                                                                <select class="form-control" id="gioitinh_edit">
	                                                                <option value="1">Nam</option>
	                                                                <option value="2">Nữ</option>
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Lớp</label>
                                                                <select class="form-control select2" id="lop_edit">
	                                                            @foreach($lop as $l)
	                                                                <option value="{{$l->idlop}}">{{$l->tenlop}}</option>
                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khoa</label>
                                                                <select class="form-control select2" id="khoa_edit">
	                                                            @foreach($khoa as $k)
	                                                                <option value="{{$k->idkhoa}}">{{$k->tenkhoa}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Chuyên ngành</label>
                                                                <select class="form-control select2" id="chuyennganh_edit">
	                                                            @foreach($chuyennganh as $cn)
	                                                                <option value="{{$cn->idcn}}">{{$cn->tenchuyennganh}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Khóa học</label>
                                                                <select class="form-control select2" id="khoahoc_edit">
	                                                            @foreach($khoahoc as $kh)
	                                                                <option value="{{$kh->idkh}}">{{$kh->nambatdau}} - {{$kh->namketthuc}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Loại hình đào tạo</label>
                                                                <select class="form-control select2" id="lhdt_edit">
	                                                            @foreach($loaihinhdaotao as $lhdt)
	                                                                <option value="{{$lhdt->idlhdt}}">{{$lhdt->tenlhdt}}</option>
	                                                            @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <input type="hidden" id="id_sv" />
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="save">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="plugins/select2/js/select2.min.js"></script>
@endsection
@section('script')
<script>
    @if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
    function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();
    $('.select2').each(function () {
                $(this).select2({
                    dropdownParent: $(this).parent()
                });
            });
    jQuery('#ngaysinh').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startView: 2,
    });
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tentk = $('#tentk').val();
        var matkhau = $('#matkhau').val();
        var hoten = $('#hoten').val();
        var email = $('#email').val();
        var diachi = $('#diachi').val();
        var ngaysinh = $('#ngaysinh').val();
        var quequan = $('#quequan').val();
        var sdt = $('#sdt').val();
        var dantoc = $('#dantoc').val();
        var gioitinh = $('#gioitinh').val();
        var cmnd = $('#cmnd').val();
        var khoa = $('#khoa').val();
        var lop = $('#lop').val();
        var lhdt = $('#lhdt').val();
        var khoahoc = $('#khoahoc').val();
        var chuyennganh = $('#chuyennganh').val();
        $.ajax({
            type: 'post',
            url: '{{route("themsv")}}',
            data: {
                tentk: tentk, matkhau: matkhau, email: email, hoten: hoten, diachi: diachi, ngaysinh: ngaysinh, quequan: quequan, sdt: sdt, dantoc: dantoc, gioitinh: gioitinh, cmnd: cmnd, khoa: khoa, lop: lop, lhdt: lhdt, khoahoc: khoahoc, chuyennganh: chuyennganh
            },
            beforeSend: function(){ 
                if(tentk == "" || matkhau == "" || hoten == "" || email == "" || diachi == "" || ngaysinh == "" || quequan == "" || sdt == "" || dantoc == "" || gioitinh == "" || cmnd == ""){
                    toastr["info"]("Hãy nhập tất cả các trường");
                    return false;
                }
                if(validateEmail(email) == 0){
                    toastr["info"]("Email không đúng");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){            
                    toastr["success"]("Thêm thành công.");
                    setTimeout('window.location.href = "{{route("dssv")}}";',1500);

                } else {
                    toastr["info"]("Tên tài khoản đã tồn tại");
                }
            }
        })
    })
    $("#datatable").on("click", ".chinhsua", function(){
        $('#preloader').fadeIn();
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: '{{route("thongtinsv")}}',
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('#preloader').fadeOut();
                $('#hoten_edit').val(resp.hoten);
                $('#diachi_edit').val(resp.diachi);
                $('#ngaysinh_edit').val(resp.ngaysinh);
                $('#email_edit').val(resp.email);
                $('#sdt_edit').val(resp.sdt);
                $('#quequan_edit').val(resp.quequan);
                $('#gioitinh_edit').val(resp.gioitinh);
                $('#dantoc_edit').val(resp.dantoc);
                $('#cmnd_edit').val(resp.cmnd);
                $('#lop_edit').val(resp.idlop).trigger('change');
                $('#chuyennganh_edit').val(resp.idchuyennganh).trigger('change');
                $('#khoahoc_edit').val(resp.idkhoahoc).trigger('change');
                $('#khoa_edit').val(resp.idkhoa).trigger('change');
                $('#lhdt_edit').val(resp.idloaihinhdaotao).trigger('change');
                $('#id_sv').val(resp.idsv);
            }
        })
    })
    // Lưu thông tin chỉnh sửa


    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var idsv = $('#id_sv').val();
        var hoten = $('#hoten_edit').val();
        var email = $('#email_edit').val();
        var diachi = $('#diachi_edit').val();
        var ngaysinh = $('#ngaysinh_edit').val();
        var quequan = $('#quequan_edit').val();
        var sdt = $('#sdt_edit').val();
        var dantoc = $('#dantoc_edit').val();
        var gioitinh = $('#gioitinh_edit').val();
        var cmnd = $('#cmnd_edit').val();
        var khoa = $('#khoa_edit').val();
        var lop = $('#lop_edit').val();
        var lhdt = $('#lhdt_edit').val();
        var khoahoc = $('#khoahoc_edit').val();
        var chuyennganh = $('#chuyennganh_edit').val();
        $.ajax({
            type: 'post',
            url: '{{route("luuchinhsuasv")}}',
            data: {
                idsv: idsv, email: email, hoten: hoten, diachi: diachi, ngaysinh: ngaysinh, quequan: quequan, sdt: sdt, dantoc: dantoc, gioitinh: gioitinh, cmnd: cmnd, khoa: khoa, lop: lop, lhdt: lhdt, khoahoc: khoahoc, chuyennganh: chuyennganh
            },
            beforeSend: function(){
                if(tentk == "" || matkhau == "" || hoten == "" || email == "" || diachi == "" || ngaysinh == "" || quequan == "" || sdt == "" || dantoc == "" || gioitinh == ""){
                    toastr["info"]("Hãy nhập tất cả các trường");
                    return false;
                }
                if(validateEmail(email) == 0){
                    toastr["info"]("Email không đúng");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){
                    toastr["success"]("Lưu thành công.");
                    setTimeout('window.location.href = "{{route("dssv")}}";',1500);

                } else {
                    
                }
            }
        })
    })
            });
        </script>
@endsection