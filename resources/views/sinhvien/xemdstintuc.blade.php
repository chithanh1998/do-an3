@extends('layout.main')
@section('css')
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                    <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Danh sách tin tức<h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;"><div class="inbox-widget slimscroll-alt" style="min-height: 302px; overflow: hidden; width: auto; height: 250px;">
                                        @foreach($tintuc as $t)
                                        <a href="sinhvien/tintuc/xem/{{$t->idtt}}">
                                            <div class="inbox-item">
                                            <div class="inbox-item-img"><img src="assets/images/tintuc.png" class="img-circle" alt=""></div>
                                                <p class="inbox-item-author">{{$t->tieude}}</p>
                                                <p class="inbox-item-text">Ngày tạo: {{$t->created_at}}</p>
                                            </div>
                                        </a>
                                        @endforeach
                                    </div><div class="slimScrollBar" style="background: rgb(152, 166, 173); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 183.879px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>

                                </div> <!-- end card -->
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>

            
                                

@endsection
@section('js')
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
$(document).ready(function () {
            });
        </script>
@endsection