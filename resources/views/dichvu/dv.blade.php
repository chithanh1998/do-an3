@extends('layout.main')
@section('css')
<link href="plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<!-- Custom box css -->
<link href="plugins/custombox/css/custombox.min.css" rel="stylesheet">
<!-- Sweet Alert -->
<link href="plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Dịch vụ </h4>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                <div class="row">
                                <button class="btn btn-info waves-effect waves-light m-b-5" style="float: right" data-toggle="modal" data-target="#con-close-modal"> <i class="fa fa-plus m-r-5"></i> <span>Thêm</span> </button>
                                    <h4 class="m-t-5 header-title"><b>Danh sách dịch vụ</b></h4>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Dịch vụ</th>
                                            <th>Loại dịch vụ</th>
                                            <th>Quản lý</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                            @foreach($dv as $d)
                                        <tr>
                                            <td>{{$d->tendv}}</td>
                                            <td>{{$d->tenloaidv}}</td>
                                            <td><a data-toggle="modal" id="{{$d->iddv}}" data-target="#con-close-modal2" class="chinhsua"><i class="fa fa-pencil"></i></a>
                                            <a href="dichvu/xoa/{{$d->iddv}}" onclick="javascript:confirmationDelete($(this));return false;"><i class="fa fa-trash"></i></a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> <!-- container -->

                </div> <!-- content -->


            </div>


            <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Thêm dịch vụ</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tên dịch vụ</label>
                                                                <input type="text" class="form-control" id="tendv" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Loại dịch vụ</label>
                                                                <select class="form-control select2" id="loaidv">
                                                                    @foreach($loaidv as $ldv)
	                                                                <option value="{{$ldv->idloaidv}}">{{$ldv->tenloaidv}}</option>
	                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="add">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->


                                    <div id="con-close-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Chỉnh sửa dịch vụ</h4>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Tên dịch vụ</label>
                                                                <input type="text" class="form-control" id="tendv_edit" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group no-margin">
                                                                <label for="field-7" class="control-label">Loại dịch vụ</label>
                                                                <select class="form-control select2" id="loaidv_edit">
                                                                @foreach($loaidv as $ldv)
	                                                                <option value="{{$ldv->idloaidv}}">{{$ldv->tenloaidv}}</option>
	                                                                @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <input type="hidden" id="id_dv" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Đóng</button>
                                                    <button type="button" class="btn btn-info waves-effect waves-light" id="save">Lưu</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->

@endsection
@section('js')
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="plugins/select2/js/select2.min.js"></script>
@endsection
@section('script')
<script>
@if(session('succ'))
    toastr["success"]("{{session('succ')}}");
@endif
@if(session('err'))
    toastr["info"]("{{session('err')}}");
@endif
function confirmationDelete(anchor) {
    swal({
                title: "Bạn chắc chắn muốn xóa?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: 'btn-warning',
                confirmButtonText: "Có",
                cancelButtonText: "Không",
                closeOnConfirm: false
            }, function () {
                window.location = anchor.attr("href"); 
            });
}
$(document).ready(function () {
    $('#datatable').dataTable();
    $('.select2').each(function () {
                $(this).select2({
                    dropdownParent: $(this).parent()
                });
            });
    $('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tendv = $('#tendv').val();
        var loaidv = $('#loaidv').val();
        $.ajax({
            type: 'post',
            url: '{{route("themdv")}}',
            data: {
                tendv: tendv, loaidv: loaidv
            },
            beforeSend: function(){ 
                if(tendv == ""){
                    toastr["info"]("Hãy nhập tên dịch vụ");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){            
                    toastr["success"]("Thêm thành công.");
                    setTimeout('window.location.href = "{{route("dsdv")}}";',1500);

                } else {
                    toastr["info"]("Tên dịch vụ đã tồn tại");
                }
            }
        })
    })
    $("#datatable").on("click", ".chinhsua", function(){
        $('#preloader').fadeIn();
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: '{{route("thongtindv")}}',
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('#preloader').fadeOut();
                $('#tendv_edit').val(resp.tendv);
                $('#loaidv_edit').val(resp.idloaidv).trigger('change');
                $('#id_dv').val(resp.iddv);
            }
        })
    })
    // Lưu thông tin chỉnh sửa
    $('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var iddv = $('#id_dv').val();
        var tendv = $('#tendv_edit').val();
        var loaidv = $('#loaidv_edit').val();
        $.ajax({
            type: 'post',
            url: '{{route("luuchinhsuadv")}}',
            data: {
                iddv: iddv, tendv: tendv, loaidv: loaidv
            },
            beforeSend: function(){
                if(tendv == ""){
                    toastr["info"]("Hãy nhập tên dịch vụ");
                    return false;
                }
                $('#preloader').fadeIn();
            },
            success: function(resp){
                $('#preloader').fadeOut();
                if(resp == "ok"){
                    toastr["success"]("Lưu thành công.");
                    setTimeout('window.location.href = "{{route("dsdv")}}";',1500);
                } else {
                    toastr["info"]("Tên dịch vụ đã tồn tại");
                }
            }
        })
    })
            });
        </script>
@endsection